<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyExchangeRate
 *
 * @property int $id
 * @property string $currency
 * @property string $base_currency
 * @property float $rate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRate whereBaseCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRate whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CurrencyExchangeRate extends Model
{
    protected $table = 'currency_exchange_rates';

    protected $guarded = ['id'];

    /**
     * @param string $currency
     * @return int|float
     */
    public static function getRateForCurrency($currency)
    {
        if ($currency === config('wlm.base_currency')) {
            return 1;
        }

        $transactionCurrencyRate = CurrencyExchangeRate::where([
            'currency'      => $currency,
            'base_currency' => config('wlm.base_currency')
        ])->firstOrFail();

        return $transactionCurrencyRate->rate;
    }

}
