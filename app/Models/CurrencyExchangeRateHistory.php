<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CurrencyExchangeRateHistory
 *
 * @property int $id
 * @property string $currency
 * @property string $base_currency
 * @property float $rate
 * @property \Carbon\Carbon $date
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRateHistory whereBaseCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRateHistory whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRateHistory whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRateHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CurrencyExchangeRateHistory whereRate($value)
 * @mixin \Eloquent
 */
class CurrencyExchangeRateHistory extends Model
{
    protected $table = 'currency_exchange_rates_history';

    protected $guarded = ['id'];

    protected $dates = ['date'];

}
