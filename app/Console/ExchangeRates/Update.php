<?php

namespace App\Console\Commands\ExchangeRates;

use App\Models\CurrencyExchangeRate;
use App\Models\CurrencyExchangeRateHistory;
use App\Services\ExchangeRates\Service;
use App\Services\ExchangeRates\Fixer\Transformer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Exception;
use DB;
use Illuminate\Support\Collection;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchange-rates:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update exchange rates';

    /**
     * @param Service     $exchangeService
     * @param Transformer $transformer
     */
    public function handle(Service $exchangeService, Transformer $transformer): void
    {
        try {

            DB::beginTransaction();

            $rates = $transformer->transform($exchangeService->get());

            $this->info('Received '.$rates->count().' rates');

            CurrencyExchangeRate::query()->delete();

            CurrencyExchangeRate::insert($rates->toArray());

            $this->addHistory($rates);

            DB::commit();

            $this->info('Succesfully saved in Database');

        } catch (Exception $exception) {

            $this->output->error($exception->getMessage());

        }
    }

    /**
     * @param Collection $rates
     * @throws Exception
     */
    private function addHistory(Collection $rates): void
    {
        CurrencyExchangeRateHistory::where('date', Carbon::today())->delete();

        $rates = $rates->map(function ($rate) {

            $result = collect($rate)->only([
                'currency',
                'base_currency',
                'rate',
            ]);

            $result['date'] = Carbon::now();

            return $result;

        });

        CurrencyExchangeRateHistory::insert($rates->toArray());
    }

}