<?php

namespace App\Services\ExchangeRates;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class Service implements ExchangeRatesContract
{

    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client;
    }

    /**
     * @return Collection
     * @throws \Exception
     */
    public function get(): Collection
    {

        return collect($this->client->latest());

    }

    /**
     * @param Carbon $date
     * @return Collection
     * @throws \Exception
     */
    public function getForDate(Carbon $date): Collection
    {
        return collect($this->client->forDate($date));
    }

}