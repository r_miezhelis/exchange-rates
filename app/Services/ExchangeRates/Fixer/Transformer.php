<?php

namespace App\Services\ExchangeRates\Fixer;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class Transformer
{

    /**
     * @param Collection $rates
     * @return Collection
     */
    public function transform(Collection $rates): Collection
    {
        return $rates->map(function ($rate, $currency) {

            return [
                'currency'      => $currency,
                'rate'          => floatval($rate),
                'base_currency' => config('services.fixer.base_currency'),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ];

        });
    }

}