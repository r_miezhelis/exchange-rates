<?php

namespace App\Services\ExchangeRates;

use Carbon\Carbon;
use Illuminate\Http\Response;
use GuzzleHttp\Client as GuzzleClient;
use Exception;

class Client
{

    /**
     * @var GuzzleClient
     */
    private $guzzleClient;

    public function __construct()
    {
        $this->guzzleClient = new GuzzleClient;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function latest(): array
    {
        return $this->request($this->makeUrl("latest"));
    }

    /**
     * @param Carbon $date
     * @return mixed
     * @throws Exception
     */
    public function forDate(Carbon $date): array
    {
        return $this->request($this->makeUrl($date->format('Y-m-d')));
    }

    /**
     * @param $url
     * @return mixed
     * @throws Exception
     */
    private function request($url): array
    {
        $response = $this->guzzleClient->get($url);

        if ($response->getStatusCode() !== Response::HTTP_OK) {

            throw new Exception('Currency exchange response problem');

        }

        $json = json_decode($response->getBody()->getContents());

        if (!$json->success || !property_exists($json, 'rates')) {

            throw new Exception('Problem with exchange rates response');

        }

        return (array)$json->rates;

    }

    /**
     * @param $path
     * @return string
     */
    private function makeUrl($path): string
    {
        $url = config('services.fixer.url') . '/' . $path . "?base=" . config('services.fixer.base_currency');
        if ($apiKey = config('services.fixer.api_key')) {
            $url .= "&access_key=" . $apiKey;
        }
        return $url;
    }

}