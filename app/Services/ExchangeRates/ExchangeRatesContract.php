<?php

namespace App\Services\ExchangeRates;

use Carbon\Carbon;
use Illuminate\Support\Collection;

interface ExchangeRatesContract
{

    /**
     * @return Collection
     */
    public function get(): Collection;

    /**
     * @param Carbon $date
     * @return Collection
     */
    public function getForDate(Carbon $date): Collection;

}